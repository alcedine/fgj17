﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MouseOverCityInfoScript : MonoBehaviour {
    public GameManagerScript gameManager;

	public GameObject cityInfoPanel;

    public Text cityName;
    public IdeologyLineScript ideologyRow1;
    public IdeologyLineScript ideologyRow2;
    public IdeologyLineScript ideologyRowNeutral;
    public Text regimeChangeLabel;

	private Camera gameCam;
	private RaycastHit hitCity;
	private CityScript hitCityInfo;

    void UpdateCityInfo()
    {
        cityInfoPanel.SetActive(true);

        // Display city name and population, e.g.:
        //   Berlin (1,390,000)
        cityName.text = hitCityInfo.cityName + " (" + hitCityInfo.totalCitizens.ToString("N00") + ")";

        // Figure out the order that ideologies will be presented
        var ideologyOrder = IdeologiesByInfluence(hitCityInfo);

        // Resize the panel so it fits the needed number of rows
        cityInfoPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(360, 60 + (16 * ideologyOrder.Count));

        // Number of ideologies in this city with one or more followers
        var numIdeologiesPresent = ideologyOrder.Count - 1;

        // Only show the number of ideology rows we need to. TODO: change the
        // position of the "neutral" row
        if (numIdeologiesPresent == 0)
        {
            ideologyRow1.gameObject.SetActive(false);
            ideologyRow2.gameObject.SetActive(false);
            ideologyRowNeutral.SetPlacement(0);
        }
        else if (numIdeologiesPresent == 1)
        {
            ideologyRow1.gameObject.SetActive(true);
            ideologyRow2.gameObject.SetActive(false);
            ideologyRow1.SetPlacement(0);
            ideologyRowNeutral.SetPlacement(1);
        }
        else
        {
            ideologyRow1.gameObject.SetActive(true);
            ideologyRow2.gameObject.SetActive(true);
            ideologyRow1.SetPlacement(0);
            ideologyRow2.SetPlacement(1);
            ideologyRowNeutral.SetPlacement(2);
        }

        // Populate ideology rows
        for (var i = 0; i < ideologyOrder.Count; ++i)
        {
            var id = ideologyOrder[i];

            var ideologyNullable = gameManager.ideologyManager.GetIdeology(id);
            if (ideologyNullable.HasValue)
            {
                var ideology = ideologyNullable.Value;

                // Select the correct row to modify 
                var row = ideologyRow1;
                if (i == 1) row = ideologyRow2;
                if (id == "neutral") row = ideologyRowNeutral;

                // Find absolute and relative influence numbers
                int influence = 0;
                if (hitCityInfo.influence.ContainsKey(id)) influence = hitCityInfo.influence[id];
                float influenceRelative = (float)(influence) / (float)(hitCityInfo.totalCitizens);

                // Signal strength is sentinel value (-1) for neutral ideology
                int signalStrength = 0;
                if (id == "neutral") signalStrength = -1;
                else if (hitCityInfo.signalStrength.ContainsKey(id)) signalStrength = hitCityInfo.signalStrength[id];

                // Make the changes to the UI components
                row.SetValues(ideology, influence, influenceRelative, signalStrength);
            }
        }

        int daysToRegimeChange = Mathf.FloorToInt(0.05f * hitCityInfo.regimeChangeCountdown);

        // Update regime change row
        if ((hitCityInfo.dominantIdeology == "communism") && (hitCityInfo.regimeIdeology != "communism"))
        {
			regimeChangeLabel.color = new Color(1.0f, 0.18f, 0.25f);
            if (hitCityInfo.regimeIdeology == "capitalism")
            {
                regimeChangeLabel.text = "Communists will overthrow the capitalist regime in " + daysToRegimeChange.ToString() + " days!";
            }
            else
            {
                regimeChangeLabel.text = "Communists will overthrow the regime in " + daysToRegimeChange.ToString() + " days!";
            }
        }
        else if ((hitCityInfo.dominantIdeology == "communism") && (hitCityInfo.regimeIdeology == "communism"))
        {
			regimeChangeLabel.color = new Color(1.0f, 0.18f, 0.25f);
            regimeChangeLabel.text = "Communist regime in place! " + hitCityInfo.GetNumNeededForRegimeChange().ToString("N00") + " capitalists needed to overthrow.";
        }
        else if (hitCityInfo.regimeIdeology == "capitalism")
        {
            if (hitCityInfo.dominantIdeology != "communism")
            {
				regimeChangeLabel.color = new Color(0, 1.0f, 0.62f);
                regimeChangeLabel.text = "Capitalist regime in place!";
            }
        }
        else if (hitCityInfo.dominantIdeology == "capitalism")
        {
			regimeChangeLabel.color = new Color(0, 1.0f, 0.62f);
            regimeChangeLabel.text = "Regime change in " + daysToRegimeChange.ToString() + " days!";
        }
        else
        {
            regimeChangeLabel.color = new Color(0.75f, 0.75f, 0.75f);
            regimeChangeLabel.text = hitCityInfo.GetNumNeededForRegimeChange().ToString("N00") + " capitalists needed for regime change.";
        }
	}

    // Returns a list of the ideologies with influence in the city, sorted by
    // influence, most to least (but "neutral" always comes last).
    List<string> IdeologiesByInfluence(CityScript city)
    {
        var ret = new List<string>();

        // Basically insertion sort here
        foreach (var p in city.influence)
        {
            // Skip the neutral demographic (it's handled separately)
            if (p.Key == "neutral") continue;

            // Skip ideologies with zero followers
            if (p.Value == 0) continue;

            // Place in front of the biggest existing ideology with less
            // influence, if any
            bool appended = false;
            for (var i = 0; i < ret.Count; ++i)
            {
                if (p.Value > city.influence[ret[i]])
                {
                    ret.Insert(0, p.Key);
                    appended = true;
                    break;
                }
            }

            // If smaller than all the others, place at the end
            if (!appended)
            {
                ret.Add(p.Key);
            }
        }

        // Neutral comes last
        ret.Add("neutral");

        // Return the result
        return ret;
    }
	
	// Use this for initialization
	void Start () {
		gameCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		if (Physics.Raycast(gameCam.ScreenPointToRay(Input.mousePosition), out hitCity))
		{
			if (hitCity.collider.gameObject.GetComponent<CityScript>() != null) {
				hitCityInfo = hitCity.collider.gameObject.GetComponent<CityScript>();
				UpdateCityInfo();
			}
		}
		else
		{
			cityInfoPanel.SetActive (false);
		}
	}
}
