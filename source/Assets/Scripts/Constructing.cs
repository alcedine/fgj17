﻿using UnityEngine;
using System.Collections;

public class Constructing : MonoBehaviour {

	private float startTime;
	public GameObject animatedTower;

	private GameObject towerRef;
	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - startTime > 5)
		{
			towerRef = Instantiate (animatedTower, transform.position, Quaternion.identity) as GameObject;
			Destroy (gameObject);
		}
	}
}
