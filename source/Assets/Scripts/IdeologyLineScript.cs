﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IdeologyLineScript : MonoBehaviour {
    public RectTransform rt;
    public Text nameLabel;
    public Image influenceBar;
    public Text influenceNumberLabel;
    public Text signalLabel;

	// Use this for initialization
	void Start()
    {
	}
	
	// Update is called once per frame
	void Update()
    {
	}

    public void SetPlacement(int index)
    {
        var top = 36 + (index * 16);
        rt.anchoredPosition = new Vector2(0, -top);
    }

    // InfluenceRelative is in [0,1].
    public void SetValues(Ideology ideology, int influence, float influenceRelative, int signalStrength=-1)
    {
        // Set the name label to the name of the ideology
        nameLabel.text = ideology.name;

        // Set the influence bar to the color of the desired ideology and
        // size it according to the relative influence (in %) of that ideology.
        // 100 px is 100% influence.
        var influenceBarWidth = 100.0f * influenceRelative;
        influenceBar.color = ideology.color;
        influenceBar.rectTransform.sizeDelta = new Vector2(100.0f * influenceRelative, 8.0f);

        // Keep the label showing the exact number of influenced citizens right
        // next to the right hand edge of the influence bar, and update its
        // text. Its color is also updated to the ideology color.
        influenceNumberLabel.color = ideology.color;
        var numLabelX = influenceBar.rectTransform.anchoredPosition.x + influenceBarWidth + 4.0f;
        influenceNumberLabel.rectTransform.anchoredPosition = new Vector2(numLabelX, 0.0f);
        influenceNumberLabel.text = influence.ToString("N00");

        // Update the signal strength label. -1 means that there is no signal 
        // (in other words: neutral ideology).
        if (signalStrength >= 0)
        {
            signalLabel.text = "+ " + signalStrength.ToString();
            signalLabel.color = ideology.color;
        }
        else signalLabel.text = "";
    }
}
