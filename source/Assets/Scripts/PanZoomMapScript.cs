﻿using UnityEngine;
using System.Collections;

public class PanZoomMapScript : MonoBehaviour {

	public float panSpeedMultiplier;

	private float panSpeed;
	private Camera panCam;

	public SpriteRenderer backgroundImage;

	// Use this for initialization
	void Start () {
		panCam = Camera.main;
		panSpeedMultiplier = 1;
		panSpeed = panSpeedMultiplier / panCam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {
		PanAndZoom ();
	}

	void PanAndZoom()
	{
		//with (mouse on edge of screen || arrow keys) && within bounds of background image -> pan camera
		if ((Input.GetKey (KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width) && panCam.ScreenToWorldPoint(new Vector3(Screen.width,0,0)).x < backgroundImage.bounds.extents.x)
		{
			panCam.transform.position += new Vector3 (0.1f * panSpeed, 0, 0);	
		}
		if ((Input.GetKey (KeyCode.LeftArrow) || Input.mousePosition.x <= 0) && panCam.ScreenToWorldPoint(new Vector3(0,0,0)).x > -backgroundImage.bounds.extents.x)
		{
			panCam.transform.position += new Vector3 (-0.1f  * panSpeed, 0, 0);
		}
		if ((Input.GetKey (KeyCode.DownArrow) || Input.mousePosition.y <= 0) && panCam.ScreenToWorldPoint(new Vector3(0,0,0)).y > -backgroundImage.bounds.extents.y)
		{
			panCam.transform.position += new Vector3 (0, -0.1f * panSpeed, 0);
		}
		if ((Input.GetKey (KeyCode.UpArrow) || Input.mousePosition.y > Screen.height) && panCam.ScreenToWorldPoint(new Vector3(0,Screen.height,0)).y < backgroundImage.bounds.extents.y)
		{
			panCam.transform.position += new Vector3 (0, 0.1f * panSpeed, 0);
		}
		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			panCam.orthographicSize = Mathf.Clamp( panCam.orthographicSize - Input.GetAxis("Mouse ScrollWheel"),1.5f, 5); //hardcoded
			panSpeed = panSpeedMultiplier / panCam.orthographicSize;
		}
	}
}

