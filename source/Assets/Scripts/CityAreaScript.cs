﻿using UnityEngine;
using System.Collections;

public class CityAreaScript : MonoBehaviour {
    public CityScript city;

    public string GetIdeology()
    {
        return city.regimeIdeology;
    }
}
