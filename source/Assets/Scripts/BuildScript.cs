﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class BuildScript : MonoBehaviour {

	//prefabs of tower, jammer
	public GameObject tower, jammer, notAllowedCross;
	//sprites or prefabs that show up next to mouse when build buttons are clicked
	public GameObject buildingTowerSprite, buildingJammerSprite;

    public Text towersLabel;
    public Text jammersLabel;

	public LayerMask mountainBlockingMask;
	//references to that small sprite, and the real thing to build (tower/jammer
	private GameObject tempBuilder, tempCross, builder;
	//to check if building is being placed
	private bool building = false;

	//to translate from screen to world position
	private Camera cam;

    //Resources, how many radio towers you can build, etc
    private bool isTower = true;
    private int towers = 5;
    private int jammers = 2;

	// Use this for initialization
	void Start () {
		building = false;
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (building)
		{
			tempBuilder.transform.position = new Vector3(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y, 0); //keep the sprite on cursor position
			tempCross.transform.position = new Vector3(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y, 0);
			tempCross.SetActive (true);
			Vector3 mouseWorldPos = cam.ScreenToWorldPoint (Input.mousePosition);
			RaycastHit2D hitCityArea = Physics2D.Raycast (mouseWorldPos, Vector2.zero); //2d raycast for city areas
			if (hitCityArea.collider != null)
			{
				if (hitCityArea.collider.gameObject.GetComponent<CityAreaScript> ().GetIdeology() == "capitalism" && Physics2D.Raycast (mouseWorldPos, Vector2.zero, 10.0f, mountainBlockingMask).collider == null) //check for player ideology and not blocked by mountains
				{
					tempCross.SetActive (false);
					if (Input.GetMouseButtonDown (0))
					{							//and when the user presses left-click mouse
						Destroy (tempBuilder);
						Destroy (tempCross);
						Instantiate (builder, new Vector3 (cam.ScreenToWorldPoint (Input.mousePosition).x, cam.ScreenToWorldPoint (Input.mousePosition).y, 0), Quaternion.identity); //build the thing
						building = false;
						if (isTower)
						{
							towers--;
							towersLabel.text = towers.ToString ();
							if (towers == 0)
								towersLabel.color = new Color (0.5f, 0.0f, 0.0f);
						}
						else
						{
							jammers--;
							jammersLabel.text = jammers.ToString ();
							if (jammers == 0)
							jammersLabel.color = new Color (0.5f, 0.0f, 0.0f);
						}
					}
				}
				else
				{
					tempCross.SetActive (true);
					if (Input.GetMouseButtonDown (0))							//and when the user presses left-click mouse
					{
						//return error sound
					}
						
				}
			}



			if (Input.GetMouseButtonDown (1)) //cancel building if right-click mouse
			{
				Destroy (tempBuilder);
				Destroy (tempCross);
				building = false;
			}
		}

	}

	public void SelectBuildTower()
	{
		//if not building something already, and if have enough resources
		if (!building && towers > 0) {
			building = true;
			tempBuilder = Instantiate (buildingTowerSprite, cam.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity) as GameObject;
			tempCross = Instantiate (notAllowedCross, cam.ScreenToWorldPoint (Input.mousePosition), Quaternion.identity) as GameObject;
			builder = tower;
            isTower = true;
		}
	}

	public void SelectBuildJammer()
	{
		//if not building something already, and if have enough resources
		if (!building && jammers > 0) {
			building = true;
			tempBuilder = Instantiate (buildingJammerSprite, cam.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity) as GameObject;
			tempCross = Instantiate (notAllowedCross, cam.ScreenToWorldPoint (Input.mousePosition), Quaternion.identity) as GameObject;
			builder = jammer;
            isTower = false;
		}
	}

	public void Restart()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name, LoadSceneMode.Single);
	}
}
