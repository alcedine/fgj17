﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityScript : MonoBehaviour {
    // Game manager.
    public GameManagerScript gameManager;

    // How many citizens are in the city, total
    public int totalCitizens = 1390000;

    // Name of the city.
    public string cityName = "Berlin";

    // Current ideology (regime) of the city.
    public string regimeIdeology = "neutral";

    // Ideology that has over 51% of the influence, which is needed for a regime
    // change. (Neutral ideology does not cause regime changes.)
    public string dominantIdeology = "neutral";

    // Number of clock ticks until the dominant ideology enacts a regime change.
    public int regimeChangeCountdown = -1;

    // The number of citizens in this city by the ideology they believe in.
    // Non-ideological citizens should go under "neutral". Use all lowercase.
    // Total should equal totalCitizens!
    public Dictionary<string, int> influence = new Dictionary<string, int>();

    // The strength of each ideology's message in the city.
    public Dictionary<string, int> signalStrength = new Dictionary<string, int>();

    // Use this for initialization
    void Start()
    {
        influence["neutral"] = totalCitizens;
        gameManager.RegisterCity(this);
	}
	
	// Update is called once per frame
	void Update()
    {
	}

    public int GetNumNeededForRegimeChange()
    {
        return Mathf.FloorToInt(0.51f * totalCitizens);
    }

    // Apply the changes in city ideology due to the current signal strength
    // (this function performs a single update cycle).
    public void UpdateInfluence()
    {
        // Compose a list of all ideologies that are participating (because the
        // set of keys in influence and the set of keys in signalStrength are
        // not necessarily equal)
        var ideologies = new List<string>();
        ideologies.Add("neutral");
        foreach (var p in influence)
        {
            if (!ideologies.Contains(p.Key))
            {
                ideologies.Add(p.Key);
            }
        }
        foreach (var p in signalStrength)
        {
            if (!ideologies.Contains(p.Key))
            {
                ideologies.Add(p.Key);
            }
        }

        // Find the base conversion rates for each ideology:
        //                s
        //   rate = A * -----
        //              B + s
        // This function starts off as linear in s and tapers off,
        // asymptotically approaching a maximum value of A. The parameter B
        // controls how soon the tapering occurs.
        var conversionRate = new Dictionary<string, float>();

        foreach (var ideology in ideologies)
        {
            if (signalStrength.ContainsKey(ideology))
            {
                const float A = 0.01f;
                const float B = 8.0f;
                float s = signalStrength[ideology];
                conversionRate[ideology] = A * (s / (B + s));
            }
        }

        // Total strength of propaganda (attenuates the effectiveness of all
        // conversion)
        var totalPropaganda = 0.0f;
        foreach (var p in signalStrength)
        {
            totalPropaganda += p.Value;
        }

        // TODO: Penalty to conversion from "not being the only game in town"

        // The base conversion rate to "neutral" (i.e., the "attrition rate")
        conversionRate["neutral"] = 0.001f;

        // Collect the influence state after this time tick into this data
        // structure
        var influenceAfterThis = new Dictionary<string, int>();

        foreach (var ideology in ideologies)
        {
            influenceAfterThis[ideology] = 0;
        }

        // For each demographic, redistribute the people's ideologies according
        // to state change probabilities
        foreach (var ideology in ideologies)
        {
            var numPeople = 0;
            if (influence.ContainsKey(ideology)) numPeople = influence[ideology];

            int numChangedTotal = 0;

            // People who switch ideologies
            foreach (var ideology2 in ideologies)
            {
                // Changes x->x don't make sense...
                if (ideology == ideology2) continue;

                // The attrition rate doesn't get attenuated by propaganda, but
                // ideologies do

                float changeRate = 0.0f;
                if (conversionRate.ContainsKey(ideology2)) changeRate = conversionRate[ideology2];
                //if (ideology2 == "neutral")
                //{
                //    changeRate = conversionRate[ideology2];
                //}
                //else
                //{
                //    if (totalPropaganda > 0.0f)
                //    {
                //        var totalEnemyPropaganda = totalPropaganda - conversionRate[ideology2];
                //        if (totalEnemyPropaganda > 0.0f)
                //        {
                //            changeRate = conversionRate[ideology2] / (1 + totalEnemyPropaganda;
                //        }
                //        else
                //        {
                //            changeRate = conversionRate[ideology2];
                //        }
                //    }
                //    else
                //    {
                //        changeRate = 0.0f;
                //    }
                //}

                // Calculate the number of people who are jumping ship
                float numChangingRaw = changeRate * numPeople;
                int numChanging = Mathf.FloorToInt(numChangingRaw);
                numChangedTotal += numChanging;

                // Apply the resultant change to the new influence table
                influenceAfterThis[ideology2] += numChanging;
            }

            // The people who didn't change must stay in the same ideology
            var peopleNotChanging = numPeople - numChangedTotal;
            influenceAfterThis[ideology] += peopleNotChanging;
        }

        // Switch to the new influence table
        influence = influenceAfterThis;

        // Macro-scale ideology
        var newDominant = "neutral";
        foreach (var id in ideologies)
        {
            if (influence[id] >= GetNumNeededForRegimeChange())
            {
                newDominant = id;
            }
        }

        // Check for neutral dominance (no regime change with neutrals)
        if (newDominant == "neutral")
        {
            dominantIdeology = newDominant;
            return;
        }

        if (newDominant == dominantIdeology)
        {
            if (dominantIdeology != regimeIdeology)
            {
                regimeChangeCountdown -= 1;
                if (regimeChangeCountdown <= 0) RegimeChange();
            }
        }
        else
        {
            dominantIdeology = newDominant;

            if (dominantIdeology != regimeIdeology)
            {
                // TODO: cue that countdown has started somewhere
                regimeChangeCountdown = 100;
            }
        }
    }

    // Adds the specified amount of strength to the accumulated broadcast
    // strength of the specified ideology, creating the field in the container
    // if necessary.
    public void AddSignal(int strength, string ideology)
    {
        if (signalStrength.ContainsKey(ideology))
        {
            signalStrength[ideology] += strength;
        }
        else
        {
           signalStrength[ideology] = strength;
        }
    }

    // Executes a regime change to the currently dominant ideology.
    void RegimeChange()
    {
        regimeIdeology = dominantIdeology;
        GetComponent<SpriteRenderer>().color = gameManager.ideologyManager.GetIdeology(regimeIdeology).Value.color;
        // TODO: newspaper, etc. info that regime change happened
		GetComponent<AudioSource>().Play();
    }
}
