﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class introVideoScript : MonoBehaviour {

	private MovieTexture moviet;
	public Material Fade;

	public Button playGameButton;
	public GameObject backgroundSound;

	bool fading = false;
	// Use this for initialization
	void Start () {
		Renderer r = GetComponent<Renderer>();
		moviet = (MovieTexture)r.material.mainTexture;
		moviet.Play();
		playGameButton.gameObject.SetActive (false);
		AudioSource aud = GetComponent<AudioSource> ();
		aud.clip = moviet.audioClip;
		aud.Play ();

		DontDestroyOnLoad (backgroundSound);
	}
	
	// Update is called once per frame
	void Update () {
		if (!fading)
		{
			if (!moviet.isPlaying)
				playGameButton.gameObject.SetActive (true);
		}
		else
		{
			if (!moviet.isPlaying)
				SceneManager.LoadScene ("Game", LoadSceneMode.Single);
		}
	}

	public void PlayGame()
	{
		Renderer r = GetComponent<Renderer>();
		r.material = Fade;
		moviet = (MovieTexture)r.material.mainTexture;
		moviet.Play();
		AudioSource aud = GetComponent<AudioSource> ();
		aud.clip = moviet.audioClip;
		aud.Play ();
		fading = true;
	}
}
