﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct Ideology
{
    // all-lowercase identifier for the ideology (e.g. "communism")
    public string id;

    // Ideology name that's shown to the user (e.g. "Communism")
    public string name;

    // Ideology adjective (e.g. "communist")
    public string adjective;

    // Lowercase string for the plural of adherents (e.g. "communists")
    public string adherentPlural;

    // Distinctive color for this ideology, for graphs
    public Color color;
}

public class IdeologyManagerScript : MonoBehaviour {
    List<Ideology> ideologies = new List<Ideology>();

	// Use this for initialization
	void Start()
    {
        // The ideology list should always contain the neutral ideology
        var neutral = new Ideology();
        neutral.id = "neutral";
        neutral.name = "Neutral";
        neutral.adjective = "neutral";
        neutral.adherentPlural = "neutrals";
        neutral.color = new Color(0.75f, 0.75f, 0.75f);
        ideologies.Add(neutral);

        // FIXME: adding capitalism and communism just to test
        var capitalism = new Ideology();
        capitalism.id = "capitalism";
        capitalism.name = "Capitalism";
        capitalism.adjective = "capitalist";
        capitalism.adherentPlural = "capitalists";
		capitalism.color = new Color(0, 1.0f, 0.62f);
        ideologies.Add(capitalism);

        var communism = new Ideology();
        communism.id = "communism";
        communism.name = "Communism";
        communism.adjective = "communist";
        communism.adherentPlural = "comrades";
        communism.color = new Color(1.0f, 0.18f, 0.25f);
        ideologies.Add(communism);
	}
	
	// Update is called once per frame
	void Update()
    {
	}

    // Finds and returns the ideology with the given id (or null, if no such
    // ideology exists).
    public Ideology? GetIdeology(string id)
    {
        foreach (var ideology in ideologies)
        {
            if (ideology.id == id)
            {
                return ideology;
            }
        }

        return null;
    }
}
