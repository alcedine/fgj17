﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum RadioTowerType { Broadcaster, Jammer };

public class RadioTowerScript : MonoBehaviour {
    // Game manager.
    public GameManagerScript gameManager;

    // Tower type (broadcaster or jammer). 
    public RadioTowerType type;

    // These containers parametrize the concentric circles that determine the
    // tower's influence. They should go from strongest to weakest, and both
    // lists should have the same number of elements.
    public List<float> signalLevelSquareRadius = new List<float>();
    public List<int> signalLevelStrength = new List<int>();

    // The ideology that this tower is spreading (or jamming).
    public string ideology = "capitalism";

    // Cumulative strength of jamming signals that affect this signal. (This
    // is only for broadcast towers.)
    public int jammingStrength;

    // Layer mask for the mountain (blocking) layer.
    const int BLOCKING_LAYER_MASK = 1 << 8;

	// Use this for initialization
	void Start()
    {
		gameManager = GameObject.Find ("Game Manager").GetComponent<GameManagerScript>();
        gameManager.RegisterRadioTower(this);
	}
	
	// Update is called once per frame
	void Update()
    {
	}

    // Returns the number of defined signal levels.
    int NumSignalLevels()
    {
        return Mathf.Max(signalLevelStrength.Count, signalLevelSquareRadius.Count);
    }

    // Returns the strength of i:th signal level, minus the jamming strength
    // (but values are limited to zero from below).
    int NetSignalLevel(int i)
    {
        return Mathf.Max(signalLevelStrength[i] - jammingStrength, 0);
    }

    // Returns the power of the signal at the given point.
    public int StrengthAt(Vector3 point)
    {
        // The tower should always have some signal levels. In case it doesn't,
        // however, the signal defaults to zero.
        if (NumSignalLevels() == 0)
        {
            return 0;
        }

        // Vector from this tower to the given point and squared 
        var direction = point - transform.position;

        // Squared distance between this tower and the point
        var r2 = direction.sqrMagnitude;

        // If the tower is right on top of the city, just return a maximum
        // signal strength value. (We also want to do this before casting the
        // ray to guard against the case where direction is a zero vector, which
        // can't be normalized.) Note that we checked NumSignalLevels above, so
        // the max strength is always defined.
        if (r2 < 0.1f) return NetSignalLevel(0);

        // Otherwise, trace a ray first to check that there aren't any mountains
        // in the way. If the signal is blocked, the strength is zero.
        var blocked = Physics2D.Raycast(transform.position, direction.normalized,
            direction.magnitude, BLOCKING_LAYER_MASK);

        if (blocked.collider)
        {
            return 0;
        }

        // Unobstructed signal; the power is the power of the smallest influence
        // radius that the distance fits inside.
        for (var i = 0; i < NumSignalLevels(); ++i)
        {
            if (r2 <= signalLevelSquareRadius[i])
            {
                return NetSignalLevel(i);
            }
        }

        // If execution reaches this point, we're outside even the weakest
        // signal radius and there is no signal.
        return 0;
    }
}
