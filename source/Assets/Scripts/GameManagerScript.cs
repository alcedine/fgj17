﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {
    // Ideology manager
    public IdeologyManagerScript ideologyManager;

    public Text dayCounterLabel;
    public Text victoryProgressLabel;

    // Day counter (one day is one tick)
    public float day = 0;
    
    // How many seconds per one clock tick
    public float tickTime;

    // How many cities captured by the player
    private int numCaptured = 0;

    // How much time left until the next time the clock should tick
    private float timeToNextTick;

    // List of Cities registered on this level
    private List<CityScript> registeredCities = new List<CityScript>();

    // List of RadioTowers of type Broadcaster registered on this level
    private List<RadioTowerScript> registeredBroadcastTowers = new List<RadioTowerScript>();

    // List of RadioTowers of type Jammer registered on this level
    private List<RadioTowerScript> registeredJammingTowers = new List<RadioTowerScript>();

	// Use this for initialization
	void Start()
    {
        // Reset the tick counter
        timeToNextTick = tickTime;
	}
	
	// Update is called once per frame
	void Update()
    {
        if (Time.deltaTime >= timeToNextTick)
        {
            // Execute a clock tick
            Tick();

            // Reset the tick timer
            timeToNextTick = timeToNextTick + tickTime - Time.deltaTime;
        }
        else
        {
            // Update the time to next clock tick
            timeToNextTick -= Time.deltaTime;
        }
	}

    // Called each clock tick.
    void Tick()
    {
        // Advance day counter
        day += 0.05f;

        // Recalculate jamming strength affecting each tower
        foreach (var broadcaster in registeredBroadcastTowers)
        {
            // Reset jamming strength accumulator
            broadcaster.jammingStrength = 0;

            // Apply jamming from all jammer towers with matching ideology
            foreach (var jammer in registeredJammingTowers)
            {
                if (jammer.ideology == broadcaster.ideology)
                {
                    var p = broadcaster.transform.position;
                    broadcaster.jammingStrength += jammer.StrengthAt(p);
                }
            }
        }

        // Recalculate broadcast strength affecting each city
        foreach (var city in registeredCities)
        {
            // Zero out the accumulator 
            city.signalStrength.Clear();

            // Apply 
            foreach (var broadcaster in registeredBroadcastTowers)
            {
                var strengthHere = broadcaster.StrengthAt(city.transform.position);
                city.AddSignal(strengthHere, broadcaster.ideology);
            }

            // Perform one influence update step
            city.UpdateInfluence();
        }

        // Count cities controlled by player
        numCaptured = 0;
        foreach (var city in registeredCities)
        {
            if (city.regimeIdeology == "capitalism") numCaptured++;
        }

        // Update UI
		dayCounterLabel.text = "Day " + ((int)day).ToString("N00");
        victoryProgressLabel.text = "Controlling " + numCaptured.ToString() + "/6 cities";

        // Check victory condition
        if (numCaptured >= 6)
        {
            // VICTORY! Go to victory screen.
            SceneManager.LoadScene("Victory", LoadSceneMode.Single);
        }

        if (numCaptured == 0)
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        }

		if (day >= 365)
		{
			SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
		}
    }

    // Add a city to the list of registered cities.
    public void RegisterCity(CityScript s)
    {
        registeredCities.Add(s);

        // Start with the frankfurt area in your control
        if (s.cityName == "Frankfurt")
        {
            s.influence["capitalism"] = 600000;
            s.influence["neutral"] = s.totalCitizens - 600000;
            s.regimeIdeology = "capitalism";
            // s.GetComponent<SpriteRenderer>().color = ideologyManager.GetIdeology("capitalism").Value.color;
        }
    }

    // Add a radio tower to the list of registered towers.
    public void RegisterRadioTower(RadioTowerScript s)
    {
        if (s.type == RadioTowerType.Broadcaster)
        {
            registeredBroadcastTowers.Add(s);
        }
        else
        {
            registeredJammingTowers.Add(s);
        }
    }
}
